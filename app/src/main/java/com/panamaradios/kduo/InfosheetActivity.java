/**
 * InfosheetActivity.java
 * Implements the app's infosheet activity
 * The infosheet activity sets up infosheet screens for "About" and "How to"
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.panamaradios.kduo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.panamaradios.kduo.helpers.TransistorKeys;


/**
 * InfosheetActivity class
 */
public final class InfosheetActivity extends AppCompatActivity {




    TextView versionTxt;
    WebView webView;
    ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Intent intent = this.getIntent();

        // set activity title
        if (intent.hasExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE)) {
            this.setTitle(intent.getStringExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE));
        }

        // set activity view
        if (intent.hasExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT) && intent.getIntExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT, -1) == TransistorKeys.INFOSHEET_CONTENT_ABOUT) {
            setContentView(R.layout.fragment_infosheet_about);
        }


//        PackageInfo pInfo = null;
//        try {
//            versionTxt = (TextView) findViewById(R.id.TV_app_version);
//            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            String version = pInfo.versionName;
//            versionTxt.setText("Version " + version);

        webView = (WebView) findViewById(R.id.webView1);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        progressBar = ProgressDialog.show(InfosheetActivity.this, "", "Loading...");

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(InfosheetActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);

                alertDialog.show();
            }
        });
        webView.loadUrl(
                "http://www.kduoapps.com/aboutus");

//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }


    }

}

/**
 * PlayerActivity.java
 * Implements the app's player activity
 * The player activity sets up the now playing view for phone  mode and inflates a menu bar menu
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.panamaradios.kduo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;

import com.panamaradios.kduo.app.MyRadio;
import com.panamaradios.kduo.core.Station;
import com.panamaradios.kduo.helpers.LogHelper;
import com.panamaradios.kduo.helpers.TransistorKeys;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;


/**
 * PlayerActivity class
 */
public final class PlayerActivity extends AppCompatActivity {

    /* Define log tag */
    private static final String LOG_TAG = PlayerActivity.class.getSimpleName();
    private MyRadio myRadio;
    public AdView adView;
    FloatingActionButton fabMenu;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    public static PlayerRecyclerAdapter mCollectionAdapter;
    private Station mStation;
    private String mStationName;
    private String mStreamUri;
    private String mImageUrl;
    private ArrayList<Object> customStations;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content view
        setContentView(R.layout.activity_player);

        fabMenu = (FloatingActionButton) findViewById(R.id.fabButon);
        fabMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view != null) {
                    onBackPressed();
                } else {
                    Intent intent = new Intent(PlayerActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        myRadio = MyRadio.getInstance();
        showAds();

        // get intent
        Intent intent = getIntent();


        // CASE: show player in phone mode
        if (intent != null && TransistorKeys.ACTION_SHOW_PLAYER.equals(intent.getAction())) {

            // get station from intent
            Station station;
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
                station = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
//                mStationList = intent.getParcelableArrayListExtra("mStationList");
                mStation = intent.getParcelableExtra(TransistorKeys.ARG_STATION);
            } else {
                station = null;
            }
            if (mStation != null) {
                mStationName = mStation.getStationName();
                mStreamUri = mStation.getStreamUri().toString();
                mImageUrl = mStation.getStationImageURL();
            } else {
                mStationName = this.getString(R.string.descr_station_name_example);
                LogHelper.e(LOG_TAG, "Error: did not receive station. Displaying default station name");
            }
            // get id of station from intent
            int stationID = 0;
            if (intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {
                stationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
            }

            // get playback action from intent (if started from shortcut)
            boolean startPlayback;
            if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE)) {
                startPlayback = intent.getBooleanExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, false);

                // enable the Up button
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                }

            } else {
                startPlayback = false;
            }

            // create bundle for player activity fragment
            Bundle args = new Bundle();
            args.putParcelable(TransistorKeys.ARG_STATION, station);
            args.putInt(TransistorKeys.ARG_STATION_ID, stationID);
            args.putBoolean(TransistorKeys.ARG_PLAYBACK, startPlayback);
            args.putParcelableArrayList("mStationList", MainActivityFragment.shortingStation);

            PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
            playerActivityFragment.setArguments(args);

            getFragmentManager().beginTransaction()
                    .add(R.id.player_container, playerActivityFragment)
                    .commit();


            myRadio.mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();

                    AdRequest adRequest = new AdRequest.Builder()
                            .build();
                    myRadio.mInterstitialAd.loadAd(adRequest);

                }
            });


            //new addition
            customStations = new ArrayList<>();
            //ad view addition
            AdRequest mAdRequest = new AdRequest.Builder()
                    .addTestDevice("07CDF722D23887F7936A5DD19CF714D0")
            //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            adView = (AdView) findViewById(R.id.myAds);
            adView.loadAd(mAdRequest);

            mRecyclerView = (RecyclerView) findViewById(R.id.player_recyclerview_collection);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
//

            try {
                for (Station stn : MainActivityFragment.shortingStation) {
                    Object nweObj = (Object) stn;
                    if (!stn.getStationName().equalsIgnoreCase(mStationName))
                        customStations.add(nweObj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mStation != null) {
                mStationName = mStation.getStationName();
                mStreamUri = mStation.getStreamUri().toString();
                mImageUrl = mStation.getStationImageURL();
            } else {
                mStationName = this.getString(R.string.descr_station_name_example);
                LogHelper.e(LOG_TAG, "Error: did not receive station. Displaying default station name");
            }
            mCollectionAdapter = new PlayerRecyclerAdapter(this, customStations, MainActivityFragment.shortingStation);

            mRecyclerView.setAdapter(mCollectionAdapter);

        }

//

    }

    @Override
    protected void onDestroy() {

        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    private void showAds() {
        if (myRadio.mInterstitialAd.isLoaded()) {
            myRadio.mInterstitialAd.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_player_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onBackPressed() {

//        if(interstitialAd.isLoaded()){
//        interstitialAd.show();
//    }
//        else{
        try {
            super.onBackPressed();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        Intent playerIntent = new Intent(this, MainActivity.class);
        startActivity(playerIntent);
//    }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // make sure that PlayerActivityFragment's onActivityResult() gets called
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {

        super.onResume();

        if (PlayerActivity.mCollectionAdapter != null) {
            PlayerActivity.mCollectionAdapter.notifyDataSetChanged();
        }
    }


}

/**
 * CollectionAdapter.java
 * Implements the CollectionAdapter class
 * A CollectionAdapter is a custom adapter for a RecyclerView
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.panamaradios.kduo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.panamaradios.kduo.app.AppStation;
import com.panamaradios.kduo.core.Station;
import com.panamaradios.kduo.helpers.LogHelper;
import com.panamaradios.kduo.helpers.TransistorKeys;
import com.google.android.gms.ads.NativeExpressAdView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * CollectionAdapter class
 */
public final class CollectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {


    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The Native Express ad view type.
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;


    /* Define log tag */
    private static final String LOG_TAG = CollectionAdapter.class.getSimpleName();
    /* Main class variables */
    private Activity mActivity;
    private SharedPreferences prefs;
    //    private final File mFolder;
    private BroadcastReceiver mPlaybackStateChangedReceiver;
    private boolean mPlayback;
    private boolean mStationLoading;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private int mStationIDSelected;
    private String currentPlayingStation;
    private boolean mTwoPane;
    private ArrayList<Station> mStationList;
    private ArrayList<Object> stations;
    private ArrayList<Object> data;
    List<AppStation> appStationList;


    /* Constructor */
    public CollectionAdapter(Activity activity, ArrayList<Object> stations, ArrayList<Station> shortingStation) {
        // set main variables
        mActivity = activity;
        this.stations = stations;
        this.mStationList = shortingStation;
        mStationIDSelected = 0;
        prefs = mActivity.getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);

    }

    public class NativeExpressAdViewHolder extends RecyclerView.ViewHolder {

        NativeExpressAdViewHolder(View view) {
            super(view);
        }
    }

    //
    @Override
    public int getItemViewType(int position) {
        return (stations.get(position) instanceof Station) ? MENU_ITEM_VIEW_TYPE : NATIVE_EXPRESS_AD_VIEW_TYPE;
    }


    public void filterFavourites(boolean showFavourites) {

        if (data == null) {
            data = stations;
        } else {
            stations = data;
        }
        if (showFavourites) {
            stations = new ArrayList<>();
            for (Object station : data) {
                if (station instanceof Station) {
                    Station mStation = (Station) station;
                    if (mStation.getCustomStation().isFavourite()) {
                        stations.add(station);
                    }
                }
            }
            if (stations.size() == 0)
                Toast.makeText(mActivity, "No Favourites added!", Toast.LENGTH_SHORT).show();
        }
        notifyDataSetChanged();

    }

    public void sortStations(ArrayList<Object> stations) {
        this.stations = stations;
        notifyDataSetChanged();
    }

    public void sortStationsReverse(){
        Collections.reverse(stations);
        notifyDataSetChanged();
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<Object> results = new ArrayList<Object>();
                if (data == null) {

//                    for (Object object : stations) {
//                        if (object instanceof Station) {
//                            Station s = (Station) object;
//                            data.add(s);
//                        } else {
//                            data.add(object);
//                        }
//                    }
                    data = stations;
                }
//                    data = stations;
                if (constraint != null) {
                    if (data != null & data.size() > 0) {
                        for (final Object g : data) {
                            if (g instanceof Station) {
                                Station s = (Station) g;
                                if (s.getStationName().toLowerCase().contains(constraint.toString().toLowerCase()))
                                    results.add(s);
                            }
//                            } else {
//                                results.add(g);
//                            }
                            if (!(g instanceof Station)) {

                                if (((String) constraint).isEmpty()) {
                                    results.add(g);
                                }
                            }

                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                stations = (ArrayList<Object>) results.values;
                notifyDataSetChanged();

            }
        }

                ;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // load state
        loadAppState(mActivity);
        // initialize broadcast receivers
        initializeBroadcastReceivers();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {


        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_collection, viewGroup, false);
                return new CollectionAdapterViewHolder(v);
            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                View nativeExpressLayoutView = LayoutInflater.from(
                        viewGroup.getContext()).inflate(R.layout.native_express_ad_container,
                        viewGroup, false);
                return new NativeExpressAdViewHolder(nativeExpressLayoutView);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder defholder, final int position) {
        // final int position --> Do not treat position as fixed; only use immediately and call holder.getAdapterPosition() to look it up later
        // get station from position

        if (stations.get(position) instanceof Station) {


            CollectionAdapterViewHolder holder = (CollectionAdapterViewHolder) defholder;
            final Station station = (Station) stations.get(position);


            if (mTwoPane && mStationIDSelected == position) {
                holder.getListItemLayout().setSelected(true);
            } else {
                holder.getListItemLayout().setSelected(false);
            }


            Glide.with(holder.itemView.getContext())
                    .load(station.getStationImageURL())
                    .override(130, 100)
                    .placeholder(R.drawable.music_place)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .dontAnimate()
                    .into(holder.getStationImageView());
            // set station name
            holder.getStationNameView().setText(station.getStationName());

            // set playback indicator - in phone view only
            if (!mTwoPane && mPlayback && (station.getPlaybackState())) {
                if (mStationLoading&&holder.getPlaybackIndicator().getVisibility()==View.GONE) {
                    holder.getPlaybackIndicator().setBackgroundResource(R.drawable.ic_playback_indicator_small_loading_24dp);
                } else {
                    holder.getPlaybackIndicator().setBackgroundResource(R.drawable.ic_playback_indicator_small_started_24dp);
                }
                holder.getPlaybackIndicator().setVisibility(View.VISIBLE);
                prefs.edit().putString(MainActivityFragment.CURRENT_RADIO, station.getStationName()).apply();
            } else {
                holder.getPlaybackIndicator().setVisibility(View.GONE);
            }

            holder.getStationFavouriteView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (station.getCustomStation().isFavourite()) {
                        removeFavoriteStation(mActivity, station.getStationName());
//                    stations.get(stations.indexOf(station)).getCustomStation().setFavourite(false);
                        station.getCustomStation().setFavourite(false);
                    } else {
                        saveFavoriteStation(mActivity, station.getStationName());
                        station.getCustomStation().setFavourite(true);
//                    stations.get(stations.indexOf(station)).getCustomStation().setFavourite(true);
                    }
                    notifyDataSetChanged();
                }
            });

            if (station.getCustomStation().isFavourite()) {
                holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_fav_true));
            } else {
                holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_fav_false));
            }


            // attach click listener
            holder.setClickListener(new CollectionAdapterViewHolder.ClickListener() {
                @Override
                public void onClick(View view, int pos, boolean isLongClick) {
                    mStationIDSelected = pos;
                    Station stnnn = (Station) stations.get(pos);
                    currentPlayingStation = stnnn.getStationName();
                    saveAppState(mActivity);
                    LogHelper.v(LOG_TAG, "Selected station (ID): " + mStationIDSelected);
                    handleSingleClick(pos, (Station) stations.get(pos));


                }
            });

        } else {

            NativeExpressAdViewHolder nativeExpressHolder =
                    (NativeExpressAdViewHolder) defholder;


            NativeExpressAdView adView =
                    (NativeExpressAdView) stations.get(position);
            ViewGroup adCardView = (ViewGroup) nativeExpressHolder.itemView;

            if (adCardView.getChildCount() > 0) {
                adCardView.removeAllViews();
            }
            if (adView.getParent() != null) {
                ((ViewGroup) adView.getParent()).removeView(adView);
            }

            adCardView.addView(adView);

        }

    }


    private void removeFavoriteStation(Context context, String stationName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        stationName = stationName + ",";
        String finalPlaylist = tempPlayList.replaceAll(stationName, "");

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, finalPlaylist);
        editor.apply();
    }


    public void saveFavoriteStation(Context context, String stationName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        StringBuilder sb = new StringBuilder();
        String tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        sb.append(tempPlayList);
        if (!sb.toString().toLowerCase().contains(stationName.toLowerCase())) {
            sb.append(stationName).append(",");
        }

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, sb.toString());
        editor.apply();

    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        if (stations != null)
            return stations.size();
        else
            return 0;
    }


    /* Handles click on list item */
    private void handleSingleClick(int position, Station station) {

//        Station station = stations.get(position);

        if (mTwoPane) {
            Bundle args = new Bundle();
            args.putParcelable(TransistorKeys.ARG_STATION, station);

            args.putInt(TransistorKeys.ARG_STATION_ID, position);
            args.putBoolean(TransistorKeys.ARG_TWO_PANE, mTwoPane);
            args.putParcelableArrayList("mStationList", mStationList);

            PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
            playerActivityFragment.setArguments(args);
            mActivity.getFragmentManager().beginTransaction()
                    .replace(R.id.player_container, playerActivityFragment, TransistorKeys.PLAYER_FRAGMENT_TAG)
                    .commit();
        } else {


            // add ID of station to intent and start activity
            Intent intent = new Intent(mActivity, PlayerActivity.class);
            intent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
            intent.putParcelableArrayListExtra("mStationList", mStationList);

            intent.putExtra(TransistorKeys.EXTRA_STATION, station);
            intent.putExtra(TransistorKeys.EXTRA_STATION_ID, position);
            mActivity.startActivity(intent);
        }


    }


    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mTwoPane = settings.getBoolean(TransistorKeys.PREF_TWO_PANE, false);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        currentPlayingStation = settings.getString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, "Choose a station below to begin..");
        mStationIDSelected = settings.getInt(TransistorKeys.PREF_STATION_ID_SELECTED, 0);
        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mStationLoading = settings.getBoolean(TransistorKeys.PREF_STATION_LOADING, false);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + ")");
    }


    /* Saves app state to SharedPreferences */
    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_SELECTED, mStationIDSelected);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, currentPlayingStation);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + " / " + mStationIDSelected + ")");
    }


    /* Setter for ID of currently selected station */
    public void setStationIDSelected(int stationIDSelected, boolean playbackState, boolean startPlayback) {
        mStationIDSelected = stationIDSelected;
        saveAppState(mActivity);
        if (mStationIDSelected >= 0 && mStationIDSelected < stations.size()) {

            Station ss = (Station) stations.get(stationIDSelected);
            ss.setPlaybackState(playbackState);
        }
        if (mTwoPane && mStationIDSelected >= 0 && mStationIDSelected < stations.size()) {

            Station ssg = (Station) stations.get(stationIDSelected);
            handleSingleClick(mStationIDSelected, ssg);
        }

        if (startPlayback) {
            // start player service using intent
            Intent intent = new Intent(mActivity, PlayerService.class);
            intent.setAction(TransistorKeys.ACTION_PLAY);
            Station stn = (Station) stations.get(stationIDSelected);
            intent.putExtra(TransistorKeys.EXTRA_STATION, stn);
            intent.putExtra(TransistorKeys.EXTRA_STATION_ID, stationIDSelected);
            mActivity.startService(intent);
            LogHelper.v(LOG_TAG, "Starting player service.");
        }
    }


    /* Setter for two pane flag */
    public void setTwoPane(boolean twoPane) {
        mTwoPane = twoPane;
    }


    /* Reloads app state */
    public void refresh() {
        loadAppState(mActivity);
    }


    /* Finds station when given its Uri */
    public Station findStation(Uri streamUri) {

        // traverse list of stations
        for (int i = 0; i < stations.size(); i++) {
            if (stations.get(i) instanceof Station) {
                Station station = (Station) stations.get(i);
                if (station.getStreamUri().equals(streamUri)) {
                    return station;
                }
            }
        }

        // return null if nothing was found
        return null;
    }


    /* Getter for ID of given station */
    public int getStationID(Station station) {
        return stations.indexOf(station);
    }


    /* Getter for station of given ID */
    public Object getStation(int stationID) {
        return stations.get(stationID);
    }

    /*



        /* Handles changes in state of playback, eg. start, stop, loading stream */
    private void handlePlaybackStateChanged(Intent intent) {

        // load app state
        loadAppState(mActivity);

        if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE) && intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {

            notifyDataSetChanged();

            // get station ID from intent
            int stationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
            switch (intent.getIntExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, 1)) {


                // CASE: player is preparing stream
                case TransistorKeys.PLAYBACK_LOADING_STATION:

                    if (mStationIDLast > -1 && mStationIDLast < stations.size()) {
                        if (stations.get(mStationIDLast) instanceof Station) {
                            Station stnn = (Station) stations.get(mStationIDLast);
                            stnn.setPlaybackState(false);
                        }
                    }
                    mStationLoading = true;
                    mPlayback = true;

                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(true);
                        }
                    }
                    notifyDataSetChanged();

                    break;

                // CASE: playback has started
                case TransistorKeys.PLAYBACK_STARTED:

                    mStationLoading = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(true);
                        }
                    }
                    notifyDataSetChanged();
                    break;

                // CASE: playback was stopped
                case TransistorKeys.PLAYBACK_STOPPED:

                    mPlayback = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(false);
                        }
                    }
                    notifyDataSetChanged();
                    break;
            }
        }

    }


    /* Initializes broadcast receivers for onCreate */
    private void initializeBroadcastReceivers() {

        // RECEIVER: state of playback has changed
        mPlaybackStateChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE)) {
                    handlePlaybackStateChanged(intent);
                }
            }
        };
        IntentFilter playbackStateChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mPlaybackStateChangedReceiver, playbackStateChangedIntentFilter);
    }


}